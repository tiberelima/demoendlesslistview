package com.tlima.demoendlesslistview;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;

import com.tlima.demoendlesslistview.adapter.EndlessAdapter;
import com.tlima.demoendlesslistview.customview.EndlessListview;
import com.tlima.demoendlesslistview.listener.EndlessListener;

import java.util.ArrayList;
import java.util.List;


public class MainActivity extends ActionBarActivity implements EndlessListener {

    private final static int TOTAL_ITEM_REQUISICAO = 50;
    private EndlessListview endlessListview;
    private EndlessAdapter endlessAdapter;
    private int page = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        endlessListview = (EndlessListview) findViewById(R.id.evLista);
        endlessAdapter = new EndlessAdapter(this, getItens(page), R.layout.row_layout);
        endlessListview.setLoadingView(R.layout.loading_layout);
        endlessListview.setAdapter(endlessAdapter);
        endlessListview.setEndlessListener(this);

    }


    @Override
    public void loadData() {
        FakeLoad fakeLoad = new FakeLoad();
        fakeLoad.execute(new String[]{});
    }

    private List<String> getItens(int page) {
        List<String> dados = new ArrayList<>();
        for (int i = 0; i < TOTAL_ITEM_REQUISICAO; i++) {
            dados.add("Item " + (i * (page + i)));
        }
        page++;
        return dados;
    }

    private class FakeLoad extends AsyncTask<String, Void, List<String>> {

        @Override
        protected List<String> doInBackground(String... params) {
            try {
                Thread.sleep(4000);
            } catch (Exception e) {

            }
            return getItens(page);
        }

        @Override
        protected void onPostExecute(List<String> strings) {
            super.onPostExecute(strings);
            endlessListview.addNewData(strings);
        }
    }
}
