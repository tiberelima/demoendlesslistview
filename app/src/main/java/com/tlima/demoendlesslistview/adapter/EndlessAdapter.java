package com.tlima.demoendlesslistview.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.tlima.demoendlesslistview.R;

import java.util.List;

public class EndlessAdapter extends ArrayAdapter<String> {

    private final List<String> lista;
    private final Context context;
    private final int layoutId;

    public EndlessAdapter(Context context, List<String> lista, int layoutId) {
        super(context, layoutId, lista);
        this.lista = lista;
        this.context = context;
        this.layoutId = layoutId;
    }

    @Override
    public int getCount() {
        return lista.size();
    }

    @Override
    public String getItem(int position) {
        return lista.get(position);
    }

    @Override
    public long getItemId(int position) {
        return lista.get(position).hashCode();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(layoutId, parent, false);
        }

        TextView texto = (TextView) convertView.findViewById(R.id.tvTexto);
        texto.setText(lista.get(position));

        return convertView;
    }
}
