package com.tlima.demoendlesslistview.listener;

public interface EndlessListener {

    public void loadData();
}
